# Nachhaltiges und Alternativen

<span style="text-decoration:underline">Aus Sicht des Datenschutzes sind so gut wie alle Seiten/Shops leider eine Katastrophe</span>   <img class="smilies" src="./images/smilies/icon_exclaim.gif" width="15" height="17" alt=":!:" title="Exclamation"> <br>
<br>
<em class="text-italics">Themen Kleidung, Bücher, Essen und Trinken, Mode, Dokus usw. </em><br>
<br>
<br>
<strong class="text-strong">Haushalt und Kosmetik</strong><br>
<br>
∙<a href="https://www.all-eco-trendigo.de/" class="postlink" rel="nofollow">all-eco-trendi-go</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.all-eco-trendigo.de%2F" class="postlink" rel="nofollow">Webbkoll</a> – keine <img class="smilies" src="./images/smilies/icon_exclaim.gif" width="15" height="17" alt=":!:" title="Exclamation"> Drittanbieter): alles fair, vegan und möglichst ökologisch<br>
∙<a href="https://www.kushel.de/" class="postlink" rel="nofollow">Kushel</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.kushel.de" class="postlink" rel="nofollow">Webbkoll</a>): Nachhaltigere Handtücher, Bademäntel &amp; Co. anteilig aus Holzfasern<br>
∙<a href="https://sauberkasten.com/" class="postlink" rel="nofollow">Sauberkasten</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fsauberkasten.com" class="postlink" rel="nofollow">Webbkoll</a>): Haushaltsreiniger selbst herstellen; Zubehör und Rezepte.<br>
∙<a href="https://www.steffis-hexenkueche.com/" class="postlink" rel="nofollow">Steffis-Hexenküche</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.steffis-hexenkueche.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): Bezahlung nur mit paypal oder Sofortüberweisung möglich  <img class="smilies" src="./images/smilies/icon_exclaim.gif" width="15" height="17" alt=":!:" title="Exclamation"> <br>
∙<a href="https://www.wolkenseifen.de/" class="postlink" rel="nofollow">Wolkenseifen</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.wolkenseifen.de%2F" class="postlink" rel="nofollow">Webbkoll</a>)<br>
<br>
<br>
<strong class="text-strong">Lebensmittel und Haushalt</strong><br>
<br>
∙<a href="https://www.naturlieferant.de/" class="postlink" rel="nofollow">naturlieferant</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.naturlieferant.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Plastikfreie Produkte<br>
<br>
<br>
<strong class="text-strong">Lebensmittel (Essen und Trinken)</strong><br>
<br>
∙<a href="https://fairafric.com/" class="postlink" rel="nofollow">fairafric</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Ffairafric.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): Bio Schokolade mit Wertschöpfung in Ghana und großem Fokus auf sozialen Wirkung vor Ort. Unterstützt und fördert dabei nachhaltigen Kakao-Anbau und produziert seit Ende 2018 klimaneutral.<br>
∙<a href="https://www.aroma-zapatista.de/" class="postlink" rel="nofollow">Kaffeekollektiv Aroma Zapatista</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.aroma-zapatista.de" class="postlink" rel="nofollow">Webbkoll</a>): Verschiedenste faire Kaffees von Kooperativen der indigenen Widerstandsbewegungen in Chiapas (Mexiko) und Cauca (Kolumbien).<br>
<br>
<br>
<strong class="text-strong">Kleidung und Schuhe</strong><br>
<br>
∙<a href="https://www.freitag.ch/" class="postlink" rel="nofollow">Freitag</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.freitag.ch%2F" class="postlink" rel="nofollow">Webbkoll</a>): Taschen und diverse Accessoires aus recycelten LKW-Planen<br>
∙<a href="https://www.grundstoff.net/" class="postlink" rel="nofollow">Grundstoff</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.grundstoff.net%2F" class="postlink" rel="nofollow">Webbkoll</a>): deutscher Shop, fair und Bio, Produktion leider nicht unbedingt in DE.<br>
∙<a href="http://lederhosenmacher.de/" class="postlink" rel="nofollow">Lerderhosen Stangassinger</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Flederhosenmacher.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Lederhosen "wie früher", Webseite bäh, immerhin 0 Cookies.<br>
∙<a href="https://www.livingcrafts.de/" class="postlink" rel="nofollow">Living Crafts</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.livingcrafts.de" class="postlink" rel="nofollow">Webbkoll</a>): Gehört zu dennree, fair und Bio ja - ansonsten naja.<br>
∙<a href="https://loveco-shop.de/ueber-uns/" class="postlink" rel="nofollow">Loveco-Shop</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Floveco-shop.de%2F" class="postlink" rel="nofollow">webkoll</a>)<br>
∙<a href="https://www.lunge.com/" class="postlink" rel="nofollow">Lunge</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Flunge.com#requests" class="postlink" rel="nofollow">Webbkoll</a>): Hochwertige, schadstoffreie und vegane Schuhe.<br>
∙<a href="https://www.manomama.de/" class="postlink" rel="nofollow">Manomama</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.manomama.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): komplett in Deutschland hergestellt, fair und Bio  <img class="smilies" src="./images/smilies/icon_exclaim.gif" width="15" height="17" alt=":!:" title="Exclamation"> <br>
∙<a href="https://www.shoezuu.de/" class="postlink" rel="nofollow">Shoezuu</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.shoezuu.de%2F" class="postlink" rel="nofollow">webkoll</a>) <br>
∙<a href="https://www.trigema.de" class="postlink" rel="nofollow">Trigema</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.trigema.de" class="postlink" rel="nofollow">Webbkoll</a>): Herstellung in Deutschland, um Nachhaltigkeit bemüht, teils Bio. Webseite leider finster.<br>
∙<a href="https://www.wildling.shoes/" class="postlink" rel="nofollow">Wildling-Schuhe</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.wildling.shoes%2F" class="postlink" rel="nofollow">Webbkoll</a>): Gestartet als Öko-Kinderschuhhersteller. Mittlerweile auch Modelle für Erwachsene. Zahlungsmöglichkeiten eher bescheiden.<br>
<br>
<br>
<strong class="text-strong">Kleidung und Wohnen</strong><br>
<br>
∙<a href="https://www.hessnatur.com/" class="postlink" rel="nofollow">hessnatur</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.hessnatur.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): haben diverse ökologische Projekte ins Leben gerufen, Webseite ziemlich verseucht<br>
<br>
<br>
<strong class="text-strong">Bücher online bestellen</strong><br>
<br>
∙<a href="https://www.buch7.de/" class="postlink" rel="nofollow">Buch7</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fbuch7.de" class="postlink" rel="nofollow">Webbkoll</a>): Ein Teil des Erlöses wird für gemeinnützige Organisationen gespendet. <br>
<br>
<br>
<strong class="text-strong">Shops für alles Mögliche</strong><br>
<br>
∙<a href="https://assmus.shop/" class="postlink" rel="nofollow">ASSMUS</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fassmus.shop%2F" class="postlink" rel="nofollow">Webbkoll</a>): Hersteller der Produkte aus DE oder Europa<br>
∙<a href="https://www.avocadostore.de/" class="postlink" rel="nofollow">Avocado Store</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.avocadostore.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Rohstoffe aus Bioanbau, Made in Germany, Cradle to Cradle, Fair &amp; Sozial, Schadstoffreduzierte Herstellung u. a.<br>
∙<a href="https://www.biber.com/" class="postlink" rel="nofollow">Biber</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.biber.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): Produkte aller Art<br>
∙<a href="https://www.fairmondo.de" class="postlink" rel="nofollow">Fairmondo</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.fairmondo.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Verkaufsplattform und Shop für nachhaltige Produkte. <br>
∙<a href="https://www.lilligreenshop.de/" class="postlink" rel="nofollow">Lilli Green Shop</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.lilligreenshop.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Produkte aller Art<br>
∙<a href="https://www.mehr-gruen.de/" class="postlink" rel="nofollow">mehr-grün</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.mehr-gruen.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Produkte aller Art<br>
∙<a href="https://plastikfreiheit.de/" class="postlink" rel="nofollow">Plastikfreiheit</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fplastikfreiheit.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Plastikfreie Produkte aller Art (Haushalt), aber nur zum Ideenholen gut, da meist auf Amazon verlinkt wird<br>
∙<a href="https://www.union-coop.org/shop/" class="postlink" rel="nofollow">Union Coop Föderation</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.union-coop.org" class="postlink" rel="nofollow">Webbkoll</a>): Bunter Mix an Produkten von gewerkschaftlich organisierten Kollektivbetrieben und Kooperativen.<br>
∙<a href="https://www.waschbaer.de/shop/" class="postlink" rel="nofollow">Waschbär</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.waschbaer.de%2Fshop%2F" class="postlink" rel="nofollow">Webbkoll</a>): großes Sortiment; nicht nur, aber einiges an Öko-Zeug dabei; Webseite leider schrecklich<br>
<br>
<br>
<strong class="text-strong">Upcycling</strong><br>
<br>
∙<a href="https://www.upcycling-deluxe.com/" class="postlink" rel="nofollow">Upcycling Deluxe</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.upcycling-deluxe.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): Produkte aller Art; handgemacht, vegan, zu mindestens 75% recycelt oder lokal hergestellt<br>
<br>
<br>
<strong class="text-strong">Banken</strong><br>
<br>
∙<a href="https://www.ethikbank.de/" class="postlink" rel="nofollow">Ethikbank</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.ethikbank.de%2F" class="postlink" rel="nofollow">Webbkoll</a>): Ableger der Volksbank; Kreditvergabe nach ethischen Grundsätzen<br>
∙<a href="https://www.gls.de/privatkunden/" class="postlink" rel="nofollow">GLS Bank</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.gls.de%2Fprivatkunden%2F" class="postlink" rel="nofollow">Webbkoll</a>): <br>
∙<a href="https://www.sozialbank.de/angebot.html" class="postlink" rel="nofollow">Bank für Sozialwirtschaft</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.sozialbank.de" class="postlink" rel="nofollow">Webbkoll</a>): Mit Abstrichen: ein klein wenig hinter den anderen hier genannten, dennoch vor den bekannten Banken<br>
∙<a href="https://www.triodos.de/privatkunden" class="postlink" rel="nofollow">Triodos Bank</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.triodos.de%2Fprivatkunden" class="postlink" rel="nofollow">Webbkoll</a>)<br>
<br>
<a href="https://anthroposophie.blog/gls-bank/" class="postlink" rel="nofollow">GLS und Triodos unterstützen</a> auch esoterische oder anthroposophische Projekte (z. B. demeter, Waldorfschulen).<br>
<br>
<br>
<strong class="text-strong">Energieanbieter ("Strom")</strong><br>
<br>
Alle u. g. werden von <a href="https://www.robinwood.de/sites/default/files/Oekostrom-Wechsel_Recherchebericht_ROBINWOOD_2016.pdf" class="postlink pdf-link" rel="nofollow">RobinWood</a> empfholen.<br>
∙<a href="https://buergerwerke.de/" class="postlink" rel="nofollow">Bürgerwerke</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fbuergerwerke.de%2F" class="postlink" rel="nofollow">Webbkoll</a>)<br>
∙<a href="https://www.ews-schoenau.de/" class="postlink" rel="nofollow">EWS-Schönau</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.ews-schoenau.de%2F" class="postlink" rel="nofollow">Webbkoll</a>)<br>
∙<a href="https://www.greenpeace-energy.de/privatkunden.html" class="postlink" rel="nofollow">Greenpeace-Enerergy</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.greenpeace-energy.de%2Fprivatkunden.html" class="postlink" rel="nofollow">Webbkoll</a>)<br>
∙<a href="https://www.naturstrom.de/" class="postlink" rel="nofollow">Naturstrom-AG</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.naturstrom.de%2F" class="postlink" rel="nofollow">Webbkoll</a>)<br>
∙<a href="https://www.polarstern-energie.de/#/privat/oekostrom" class="postlink" rel="nofollow">Polarstern-Energie</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.polarstern-energie.de%2F" class="postlink" rel="nofollow">Webbkoll</a>)<br>
<br>
<br>
<strong class="text-strong">Versicherungen</strong><br>
<br>
∙<a href="https://www.ver.de" class="postlink" rel="nofollow">ver.de</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fver.de" class="postlink" rel="nofollow">Webbkoll</a>): Eine Alternative zu klassischen Versicherungsmodellen; Seite wird angeblich gerade überarbeitet<br>
<br>
<br>
<strong class="text-strong">Informationsseiten</strong><br>
<br>
∙<a href="https://www.derknauserer.at/" class="postlink" rel="nofollow">Der Knauserer</a> (<a href="https://webbkoll.dataskydd.net/en/resultsurl=http%3A%2F%2Fwww.derknauserer.at%2F" class="postlink" rel="nofollow">Webbkoll</a>): Newsletter, Forum und ein Riesenarchiv zum Thema Nachhaltigkeit auch für nicht Knauserer<br>
∙<a href="https://www.hilfswerft.de/poster-nachhaltiger-konsum-die-nachhaltigen-222-deutschlands/" class="postlink" rel="nofollow">Hilfswerft – Poster Nachhaltiger Konsum</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fwww.hilfswerft.de%2Fposter-nachhaltiger-konsum-die-nachhaltigen-222-deutschlands%2F" class="postlink" rel="nofollow">Webbkoll</a>): Sammlung nachhaltiger und fairer Hersteller/Produkte als Poster von der Hilfswerft<br>
∙<a href="https://www.smarticular.net" class="postlink" rel="nofollow">Smarticular</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.smarticular.net%252" class="postlink" rel="nofollow">Webbkoll</a>): Verschiedene Themen die sich mit Nachhaltigkeit befassen. Leider ebenfalls voll mit Drittanbietern. <br>
∙<a href="https://utopia.de" class="postlink" rel="nofollow">Utopia</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Futopia.de" class="postlink" rel="nofollow">Webbkoll</a>): Sehr umfangreiche Infoseite zu verschiedensten Themen. Leider ein third Party Albtraum, sehr verseucht. Und es gibt keine sinnvollen rss feeds mehr. <br>
<br>
<br>
<strong class="text-strong">Filme und Dokus</strong><br>
<br>
∙<a href="https://www.zdf.de/comedy/die-anstalt/die-anstalt-vom-18-dezember-2018-100.html" class="postlink" rel="nofollow">Die Anstalt vom 18.12.2018</a>: geniale Folge in der es um Fleischkonsum, Verbrauchertäuschung und die Folgen geht. <br>
∙<a href="http://www.whatthehealthfilm.com/" class="postlink" rel="nofollow">What the Health</a>  (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.whatthehealthfilm.com%2F" class="postlink" rel="nofollow">Webbkoll</a>): Warum eine pflanzliche Ernährung gut für alle ist; verfügbar u.a. auf Netflix.<br>
∙<a href="https://plasticoceans.org/about-film/" class="postlink" rel="nofollow">Plastic Oceans</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fplasticoceans.org" class="postlink" rel="nofollow">Webbkoll</a>): Der Titel ist hier gleich Programm. Einfach erschreckend wie Plastik jegliche Gewässer vergiftet; verfügbar u.a. auf Netflix.<br>
∙<a href="https://de.wikipedia.org/wiki/Plastic_Planet" class="postlink" rel="nofollow">Plastic Planet</a>: Die Gefahren von Plastik<br>
∙<a href="https://de.wikipedia.org/wiki/We_Feed_the_World" class="postlink" rel="nofollow">We Feed the World</a>: Zur Massenproduktion von Lebensmitteln, Massentierhaltung<br>
∙<a href="https://de.wikipedia.org/wiki/Super_Size_Me" class="postlink" rel="nofollow">Super Size Me</a>: Kritik an Fast-Food-Ketten und ein McDonald's-Selbstversuch<br>
∙<a href="https://de.wikipedia.org/wiki/Food,_Inc." class="postlink" rel="nofollow">Food, Inc.</a>: Wie die Lebensmittelindustrie in den USA monopolisiert und von Unternehmen kontrolliert wird<br>
∙<a href="https://de.wikipedia.org/wiki/Alphabet_%28Film%29" class="postlink" rel="nofollow">Alphabet</a>: Nicht ganz passend, aber sehenswert - Ein kritischer Blick auf unser Bildungssystem<br>
∙<a href="https://underourskin.com/" class="postlink" rel="nofollow">Under Our Skin</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Funderourskin.com" class="postlink" rel="nofollow">Webbkoll</a>):  Doku über den "Borreliose-Krieg"<br>
∙<a href="https://invidio.us/watch?v=ndBdXBs2vIU" class="postlink" rel="nofollow">Der Tüftler-König von Deutschland</a>: Doku über die Reparierbarkeit und Langlebigkeit der Dinge unseres Alltags.<br>
<br>
<br>
<strong class="text-strong">Sonstiges</strong><br>
<br>
∙<a href="https://www.plantacionesedelman.com/werde-baumpate/" class="postlink" rel="nofollow">Plantaciones Regenwald verschenken</a> (<a href="https://webbkoll.dataskydd.net/en/results?url=http%3A%2F%2Fwww.plantacionesedelman.com%2Fwerde-baumpate%2F" class="postlink" rel="nofollow">webbkoll</a>): Baumpatenschaft<br>
∙<a href="https://snap-pap.de/" class="postlink" rel="nofollow">SnapPap</a> (<a href="https://webbkoll.dataskydd.net/de/results?url=http%3A%2F%2Fsnap-pap.de%2F" class="postlink" rel="nofollow">webkoll</a>): abwaschbares, wiederverwendbares Papier</div>
